import ssd1306
import framebuf
from machine import I2C, Pin, freq
from ulab import numpy as np
import random
import time


def step(grid):
    """
    computes one step of Conway's game of life on a toroidal 2D grid
    """
    count = np.zeros(grid loo.shape, dtype=np.uint8)
    count[:, :] = grid
    # sum all 8 neighbors
    count += np.roll(grid, 1, axis=0) + np.roll(grid, -1, axis=0)
    count += np.roll(grid, 1, axis=1) + np.roll(grid, -1, axis=1)
    count += np.roll(np.roll(grid, 1, axis=0), 1, axis=1) + np.roll(np.roll(grid, -1, axis=0), 1, axis=1)
    count += np.roll(np.roll(grid, 1, axis=0), -1, axis=1) + np.roll(np.roll(grid, -1, axis=0), -1, axis=1)
    # logical operator encoding the rules
    return (count == 3) | (grid & (count == 4))


def init_grid(grid):
    height, width = grid.shape

    # random initialization
    for i in range(height):
        for j in range(width):
            grid[i, j] = random.getrandbits(1)


def main():
    freq(250000000)

    i2c = I2C(scl=Pin(7), sda=Pin(6))
    display = ssd1306.SSD1306_I2C(128, 64, i2c)

    width = 128
    height = 64

    a = np.zeros((height, width), dtype=np.bool)

    init_grid(a)

    i = 0
    while True:
        t1 = time.ticks_ms()
        display.fill(0)
        fb = framebuf.FrameBuffer(a.tobytes(), width, height, framebuf.GS8)
        display.blit(fb, 0, 0)
        display.show()
        a = step(a)
        t2 = time.ticks_ms()
        i += 1
        if i == 10:
            print(f"dt: {time.ticks_diff(t2, t1)} ms")
            i = 0


if __name__ == "__main__":
    main()
